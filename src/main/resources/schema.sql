DROP TABLE IF EXISTS entities;
DROP SEQUENCE IF EXISTS global_sequence;

CREATE SEQUENCE global_sequence START 1000;

CREATE TABLE entities (
  id           INTEGER PRIMARY KEY DEFAULT nextval('GLOBAL_SEQUENCE'),
  content      VARCHAR(1024)        NOT NULL,
  creationDate TIMESTAMP            NOT NULL
);