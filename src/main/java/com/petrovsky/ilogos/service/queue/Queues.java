package com.petrovsky.ilogos.service.queue;

import com.petrovsky.ilogos.model.Entity;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Queues
 * Class provides the collection of queue constants
 *
 * @author Sergey Petrovsky
 */
public class Queues {

    public static final BlockingQueue<Path> READ_FILES_QUEUE = new ArrayBlockingQueue<>(1024 *1024);
    public static final BlockingQueue<Path> VALID_FILES_QUEUE = new ArrayBlockingQueue<>(1024 *1024);
    public static final BlockingQueue<Entity> PROCESSED_FILES_QUEUE = new ArrayBlockingQueue<>(1024 *1024);
}
