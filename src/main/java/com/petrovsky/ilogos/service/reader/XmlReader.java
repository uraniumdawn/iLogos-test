package com.petrovsky.ilogos.service.reader;

import com.petrovsky.ilogos.service.queue.Queues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.nio.file.*;
import java.util.stream.Stream;

/**
 * XmlReader
 * Class provides reading and moving XML files to destination folder
 *
 * @author Sergey Petrovsky
 */
public class XmlReader implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlReader.class);

    ApplicationContext applicationContext;
    private String inputDir;
    private String outputDir;

    public XmlReader(ApplicationContext applicationContext, String inputDir, String outputDir) {
        this.applicationContext = applicationContext;
        this.inputDir = inputDir;
        this.outputDir = outputDir;
    }

    @Override
    public void run() {
        try(Stream<Path> paths = Files.walk(Paths.get(applicationContext.getResource(inputDir).getFilename()))) {
            paths.forEach(path -> {
                if (Files.isRegularFile(path) && (path.toString().endsWith(".xml") || ((path.toString().endsWith(".XML"))))) {
                    Path newPath = Paths.get(applicationContext.getResource(outputDir).getFilename()
                            + FileSystems.getDefault().getSeparator() + path.getFileName());
                    try {
                        Files.copy(path, newPath, StandardCopyOption.REPLACE_EXISTING);
                        Files.delete(path);
                        LOGGER.debug("File {} was read and move to destination folder", path.toString());
                        Queues.READ_FILES_QUEUE.put(newPath);
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
