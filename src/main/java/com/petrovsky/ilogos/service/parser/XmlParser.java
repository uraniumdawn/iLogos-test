package com.petrovsky.ilogos.service.parser;

import com.petrovsky.ilogos.model.Entity;
import com.petrovsky.ilogos.service.queue.Queues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * XmlSeparator
 * Class provides parsing XML file to Entity object
 *
 * @author Sergey Petrovsky
 */
public class XmlParser implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlParser.class);

    private Lock lock = new ReentrantLock();

    @Override
    public void run() {
//        while(true) {
            lock.lock();
            try {
                Path path = Queues.VALID_FILES_QUEUE.take();
                JAXBContext jaxbContext = JAXBContext.newInstance(Entity.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                LOGGER.debug("File {} was processed and sent to Saver", path.toString());
                Entity entity = (Entity) jaxbUnmarshaller.unmarshal(new File(path.toUri()));
                Queues.PROCESSED_FILES_QUEUE.put(entity);
            } catch (JAXBException | InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
//        }
    }
}
