package com.petrovsky.ilogos.service.saver;

import com.petrovsky.ilogos.model.Entity;
import com.petrovsky.ilogos.repository.EntityRepository;
import com.petrovsky.ilogos.service.queue.Queues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * XmlSaver
 * Class provides saving Entity object to Repository
 *
 * @author Sergey Petrovsky
 */
@Component
@Scope(value = "prototype")
public class XmlSaver implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlSaver.class);

    @Autowired
    private EntityRepository entityRepository;

    private Lock lock = new ReentrantLock();

    @Override
    public void run() {
        lock.lock();
        try {
            Entity entity;
//            while(true) {
                entity = Queues.PROCESSED_FILES_QUEUE.take();
                entityRepository.save(entity);
                LOGGER.debug("Entity {} taken from queue and pass to Repository", entity.toString());
//            }
        } catch (SQLException | InterruptedException e) {
            e.printStackTrace();
        }
        lock.unlock();
    }
}
