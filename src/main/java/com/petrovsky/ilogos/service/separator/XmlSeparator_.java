package com.petrovsky.ilogos.service.separator;

import com.petrovsky.ilogos.Application;
import com.petrovsky.ilogos.conf.properties.ApplicationProperties;
import com.petrovsky.ilogos.model.Entity;
import com.petrovsky.ilogos.service.queue.Queues;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

/**
 * XmlSeparator
 * Class provides separating XML file from input directory
 *
 * @author Sergey Petrovsky
 */
@Component
public class XmlSeparator_ implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private Lock lock = new ReentrantLock();

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run() {
        try(Stream<Path> paths = Files.walk(Paths.get(applicationContext.getResource(applicationProperties.getInputDir()).getFilename()))) {
            paths.forEach(path -> {
                lock.lock();
                if (Files.isRegularFile(path) && (path.toString().endsWith(".xml") || ((path.toString().endsWith(".XML"))))) {
                    try {
                        DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        File file = new File(path.toString());
                        Document document = parser.parse(file);
                        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                        Source schemaFile = new StreamSource(applicationContext.getResource(applicationProperties.getXsdFileLocation()).getFile());
                        Schema schema = factory.newSchema(schemaFile);
                        Validator validator = schema.newValidator();

                        try {
                            validator.validate(new DOMSource(document));
                            JAXBContext jaxbContext = JAXBContext.newInstance(Entity.class);
                            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                            LOGGER.debug("File {} was processed and move to destination folder", file.toString());
                            Entity entity = (Entity) jaxbUnmarshaller.unmarshal(file);
                            Queues.PROCESSED_FILES_QUEUE.put(entity);
                            FileUtils.copyFileToDirectory(file, applicationContext.getResource(applicationProperties.getOutputDir()).getFile());
                            FileUtils.deleteQuietly(file);
                        } catch (SAXException e) {
                            try {
                                LOGGER.debug("File {} was rejected and move to destination folder", file.toString());
                                FileUtils.copyFileToDirectory(file, applicationContext.getResource(applicationProperties.getRejectedDir()).getFile());
                                FileUtils.deleteQuietly(file);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } catch (IOException | JAXBException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    } catch (ParserConfigurationException | SAXException | IOException e) {
                        e.printStackTrace();
                    }
                }
                lock.unlock();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
