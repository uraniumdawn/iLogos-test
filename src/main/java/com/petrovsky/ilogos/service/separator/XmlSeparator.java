package com.petrovsky.ilogos.service.separator;

import com.petrovsky.ilogos.service.queue.Queues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * XmlSeparator
 * Class provides separation and validation XML
 *
 * @author Sergey Petrovsky
 */
public class XmlSeparator implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlSeparator.class);

    private Lock lock = new ReentrantLock();

    private ApplicationContext applicationContext;
    private String rejectedDir;
    private String xsdFileLocation;

    public XmlSeparator(ApplicationContext applicationContext, String rejectedDir, String xsdFileLocation) {
        this.applicationContext = applicationContext;
        this.rejectedDir = rejectedDir;
        this.xsdFileLocation = xsdFileLocation;
    }

    @Override
    public void run() {
//        while(true) {
            lock.lock();
            Path path = null;
            try {
                path = Queues.READ_FILES_QUEUE.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = parser.parse(new File(path.toUri()));
                SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                Source schemaFile = new StreamSource(applicationContext.getResource(xsdFileLocation).getFile());
                Schema schema = factory.newSchema(schemaFile);
                Validator validator = schema.newValidator();
                validator.validate(new DOMSource(document));
                LOGGER.debug("File {} was rejected and move to destination folder", path.toString());
                Queues.VALID_FILES_QUEUE.put(path);

            } catch (SAXException e) {
                Path newPath = Paths.get(applicationContext.getResource(rejectedDir).getFilename()
                        + FileSystems.getDefault().getSeparator() + path.getFileName());
                try {
                    Files.copy(path, newPath, StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(path);
                    LOGGER.debug("File {} was rejected and move to destination folder", path.toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } catch (IOException | ParserConfigurationException | InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
//        }

    }
}
