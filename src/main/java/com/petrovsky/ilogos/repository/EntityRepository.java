package com.petrovsky.ilogos.repository;

import com.petrovsky.ilogos.model.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

/**
 * Entity Repository
 * Repository that holds all requests to the database
 *
 * @author Sergey Petrovsky
 */
@Repository
public class EntityRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityRepository.class);

    private static final String SAVE_ENTITY = "INSERT INTO entities (content, creationDate) VALUES (?, ?)";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private DataSource dataSource;

    public void save (Entity entity) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(SAVE_ENTITY);
            preparedStatement.setString(1, entity.getContent());
            preparedStatement.setTimestamp(2, Timestamp.valueOf(entity.getCreationDate().format(formatter)));
            preparedStatement.executeUpdate();
        LOGGER.debug("Entity {} was saved to DB", entity.toString());
        connection.close();
        preparedStatement.close();
    }
}
