package com.petrovsky.ilogos.conf;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * LocalDateTimeXmlAdapter
 * LocalDateTimeXmlAdapter provides mechanism of marshaling/unmarshaling LocalDateTimeClass
 *
 * @author Sergey Petrovsky
 */
public class LocalDateTimeXmlAdapter extends XmlAdapter<String, LocalDateTime> {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public LocalDateTime unmarshal(String string) throws Exception {
        return LocalDateTime.parse(string, formatter);
    }

    @Override
    public String marshal(LocalDateTime v) throws Exception {
        /*NOP*/
        return null;
    }
}
