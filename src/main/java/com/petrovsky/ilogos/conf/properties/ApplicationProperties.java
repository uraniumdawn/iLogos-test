package com.petrovsky.ilogos.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Properties
 * Properties class for application that handle all properties
 *
 * @author Sergey Petrovsky
 */
@Component
@ConfigurationProperties(prefix = "app")
public class ApplicationProperties {

    private String inputDir;
    private String outputDir;
    private String rejectedDir;
    private String xsdFileLocation;
    private Long timeOfMonitoring;
    private Integer separatorMaximumPoolSize;
    private Integer saverMaximumPoolSize;

    public String getInputDir() {
        return inputDir;
    }

    public void setInputDir(String inputDir) {
        this.inputDir = inputDir;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public String getRejectedDir() {
        return rejectedDir;
    }

    public void setRejectedDir(String rejectedDir) {
        this.rejectedDir = rejectedDir;
    }

    public String getXsdFileLocation() {
        return xsdFileLocation;
    }

    public void setXsdFileLocation(String xsdFileLocation) {
        this.xsdFileLocation = xsdFileLocation;
    }

    public Long getTimeOfMonitoring() {
        return timeOfMonitoring;
    }

    public void setTimeOfMonitoring(Long timeOfMonitoring) {
        this.timeOfMonitoring = timeOfMonitoring;
    }

    public Integer getSeparatorMaximumPoolSize() {
        return separatorMaximumPoolSize;
    }

    public void setSeparatorMaximumPoolSize(Integer separatorMaximumPoolSize) {
        this.separatorMaximumPoolSize = separatorMaximumPoolSize;
    }

    public Integer getSaverMaximumPoolSize() {
        return saverMaximumPoolSize;
    }

    public void setSaverMaximumPoolSize(Integer saverMaximumPoolSize) {
        this.saverMaximumPoolSize = saverMaximumPoolSize;
    }
}
