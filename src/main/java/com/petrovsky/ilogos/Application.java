package com.petrovsky.ilogos;

import com.petrovsky.ilogos.conf.properties.ApplicationProperties;
import com.petrovsky.ilogos.service.parser.XmlParser;
import com.petrovsky.ilogos.service.reader.XmlReader;
import com.petrovsky.ilogos.service.saver.XmlSaver;
import com.petrovsky.ilogos.service.separator.XmlSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Application
 * Entry point class for starting application
 *
 * @author Sergey Petrovsky
 */
@SpringBootApplication
public class Application implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    private XmlSaver xmlSaver;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Starting Read thread pool");
        Executors.newScheduledThreadPool(1).scheduleWithFixedDelay(new XmlReader(applicationContext, applicationProperties.getInputDir(), applicationProperties.getOutputDir()),
                0L, applicationProperties.getTimeOfMonitoring(), TimeUnit.SECONDS);

        LOGGER.info("Starting Separator thread pool");
        ExecutorService executorService = Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(2).execute(() -> {
            while(true) executorService.execute(new XmlSeparator(applicationContext, applicationProperties.getRejectedDir(),
                        applicationProperties.getXsdFileLocation()));
        });


        LOGGER.info("Starting Parser thread pool");
        ExecutorService parserExecutorService = Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(2).execute(() -> {
            while(true) parserExecutorService.execute(new XmlParser());
        });

        LOGGER.info("Starting Saver thread pool");
        ExecutorService saverExecutorService = Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(2).execute(() -> {
            while(true) saverExecutorService.execute(xmlSaver);
        });
//        Executors.newCachedThreadPool().execute(xmlSaver);
    }
}
