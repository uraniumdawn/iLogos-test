package com.petrovsky.ilogos.model;

import com.petrovsky.ilogos.conf.LocalDateTimeXmlAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

/**
 * Entity
 * Entity class represents structure of input XML file
 *
 * @author Sergey Petrovsky
 */
@XmlRootElement(name = "Entry")
public class Entity {

    private String content;
    private LocalDateTime creationDate;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @XmlJavaTypeAdapter(LocalDateTimeXmlAdapter.class)
    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "content='" + content + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
