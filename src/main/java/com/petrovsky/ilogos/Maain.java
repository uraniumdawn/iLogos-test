package com.petrovsky.ilogos;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 * Created by uranium on 18.08.17.
 */
public class Maain {
    public static void main(String[] args) {
        Container container1 = new Container("1", true);
        Container container2 = new Container("2", false);
        Container container3 = new Container("3", false);
        Container container4 = new Container("4", true);

        List<Container> containers = new ArrayList<>();
        containers.add(container1);
        containers.add(container2);
        containers.add(container3);
        containers.add(container4);

        final Map<Boolean, Function<Set<Container>, Set<String>>> functions =
                new HashMap<Boolean, Function<Set<Container>, Set<String>>>() {{
                    put(true, set -> set
                            .stream()
                            .map(Container::getString)
                            .map(s -> s + "PoSEMACHU")
                            .collect(toSet())
                    );
                    put(false, set -> set
                            .stream()
                            .map(Container::getString)
                            .map(s -> s + "NePoSEMACHU")
                            .collect(toSet())
                    );
                }};

        final Function<Map.Entry<Boolean, Set<Container>>, Set<String>> mapToSEMA =
                entry -> functions.get(entry.getKey()).apply(entry.getValue());

        final Map<Boolean, Set<String>> processed = containers
                .stream()
                .collect(Collectors.partitioningBy(Container::getFlag, Collectors.toSet()))
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, mapToSEMA));

        System.out.println(processed);
    }
}
