package com.petrovsky.ilogos;

/**
 * Created by uranium on 18.08.17.
 */
public class Container {
    private String string;
    private Boolean flag;

    public Container(String string, Boolean flag) {
        this.string = string;
        this.flag = flag;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}
